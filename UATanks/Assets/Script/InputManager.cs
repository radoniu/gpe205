﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour {

    public enum InputScheme { WASD, arrowKeys };
    public InputScheme input = InputScheme.arrowKeys;

    public MotorManager motor;
    public TankData data;
    private GameManager gm;

    private void Start()
    {
        GameObject gameObj = GameObject.FindWithTag("GameManager");
        gm = gameObj.GetComponent<GameManager>();
    }

    void FixedUpdate () {

        switch (input)
        {
            case InputScheme.arrowKeys:
                if (Input.GetKey(KeyCode.UpArrow))
                {
                    motor.Move(data.moveSpeed);
                }
                if (Input.GetKey(KeyCode.DownArrow))
                {
                    motor.Move(-data.moveSpeed);
                }
                if (Input.GetKey(KeyCode.RightArrow))
                {
                    motor.Turn(data.turnSpeed);
                }
                if (Input.GetKey(KeyCode.LeftArrow))
                {
                    motor.Turn(-data.turnSpeed);
                }
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    motor.Shoot();
                }
                break;
        }
    }    
}
