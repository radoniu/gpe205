﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankData : MonoBehaviour {

    // Data 
    public float health = 100;
    public float moveSpeed = 3; // meters
    public float turnSpeed = 180; // degrees
    public float timeDelay = 2;
    public float noise = 3; // noise
    public float fieldOfView = 110;
    public float damage = 25;
    public Transform bulletFirePoint;
    public GameObject bulletPrefab;


    private GameManager gm;

    private void Awake()
    {
        GameObject gameObj = GameObject.FindWithTag("GameManager");
        gm = gameObj.GetComponent<GameManager>();
        
    }
}


