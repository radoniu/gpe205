﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class PowerUp {
    public float speedModifier;
    public float healthModifier;
    public float damageModifier;
    public float duration;
    public bool isPermanent;

    public void OnActivate(TankData target)
    {
        target.moveSpeed += speedModifier;
        target.health += healthModifier;
        target.damage += damageModifier;
    }

    public void OnDeactivate(TankData target)
    {
        target.moveSpeed -= speedModifier;
        target.health -= healthModifier;
        target.damage -= damageModifier;
    }
}
