﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {
    public float damage = 25;
    public float force = 500;
    public float bulletLife = 4; // longer bullet life
    public Rigidbody bulletRigidbody;
    [HideInInspector]
    public GameObject parent;


    void Start () {

		bulletRigidbody.AddForce(transform.forward * force);

        Destroy(bulletRigidbody.gameObject, bulletLife);

        parent = this.transform.parent.gameObject;
        transform.parent = null;
    }

    private void OnTriggerEnter(Collider hitTarget)
    {
        if (hitTarget.gameObject.tag.Equals("Enemy"))
        {
            hitTarget.GetComponent<TankData>().health -= damage;
            if (hitTarget.GetComponent<TankData>().health <= 0)
            {
                Destroy(hitTarget.gameObject);
            }
        }

        Destroy(gameObject);
       
    }
}
