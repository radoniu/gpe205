﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotorManager : MonoBehaviour {

    [SerializeField]
    private CharacterController characterController;

    public TankData data;
    public Transform tf;
    private float lastEventTime; // timer


    private void Awake()
    {
        tf = gameObject.GetComponent<Transform>();
        characterController = gameObject.GetComponent<CharacterController>();
        data = gameObject.GetComponent<TankData>();
       
        // save time 00:00
        lastEventTime = Time.time;
    }
 
    public void Move(float moveSpeed)
    {
        //Move character in specified direction.
        characterController.SimpleMove(tf.forward * moveSpeed);
    }
    public void Turn(float turnSpeed)
    {
        // Turn character at specified speed.
        tf.Rotate((Vector3.up * turnSpeed * Time.deltaTime), Space.Self);
    }
    public void Shoot()
    {
        //check time pased since started or last time it was pressed.
        if (Time.time >= lastEventTime + data.timeDelay)
        {
            Instantiate(data.bulletPrefab, data.bulletFirePoint.position, data.bulletFirePoint.rotation);

            // save last time it was called
            lastEventTime = Time.time;
        }
    }
    public  bool RotateTowards(Vector3 target, float speed)
    {
        Vector3 vectorToTarget;
        vectorToTarget = target - tf.position;

        Quaternion targetRotation = Quaternion.LookRotation(vectorToTarget);
        if (targetRotation != tf.rotation)
        {
            tf.rotation = Quaternion.RotateTowards(tf.rotation, targetRotation, data.turnSpeed * Time.deltaTime);

            return true;
        }

        return false;
    }


}
