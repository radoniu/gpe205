﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpControler : MonoBehaviour {

    public TankData data;
    public List<PowerUp> powerups;

    private void Start()
    {
        powerups = new List<PowerUp>();

    }
    void Update()
    {
        // Hold Expiered Powers
        List<PowerUp> expiredPowerups = new List<PowerUp>();

        // Add Powerups to tank
        foreach (PowerUp power in powerups)
        {
            // Subtract from the timer
            power.duration -= Time.deltaTime;

            // List of expired powerups
            if (power.duration <= 0)
            {
                expiredPowerups.Add(power);
            }
        }
        // Remove Powerups from tank
        foreach (PowerUp power in expiredPowerups)
        {
            power.OnDeactivate(data);
            powerups.Remove(power);
        }
        //Garbage colection
        expiredPowerups.Clear();
    }
    public void Add(PowerUp powerup)
    {
        powerup.OnActivate(data);
        powerups.Add(powerup);

        if (!powerup.isPermanent)
        {
            powerups.Add(powerup);
        }
    }
}


