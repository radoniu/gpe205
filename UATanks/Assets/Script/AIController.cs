﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour {

    public enum Personality {Normal, Scared, Chaser, Gaurd};
    public Personality personality;

    public enum AttackMode {Chase, Flee, Shoot, Normal}
    public AttackMode attackMode;

    public enum PatrollType {Stop, Loop, PingPong};
    public PatrollType patrollType;

    public Transform[] wayPoints;
    public TankData data;
    public MotorManager motor;
    public Transform target;
    public GameObject player;
    public GameObject scared;
    private Transform tf;

    private int currentPoint = 0;
    private int avoidanceStage = 0;
    private float exitTime;
    private bool isPatrolForward = true;

    public float chaseDistance = 2;
    public float fleeDistance = 1;
    public float avoidanceTime = 2;
    public float closeEnough = 1;

    public void Awake()
    {
        tf = gameObject.GetComponent<Transform>();
        data = gameObject.GetComponent<TankData>();
        motor = gameObject.GetComponent<MotorManager>();
        player = GameObject.FindGameObjectWithTag("Player");
        scared = GameObject.FindGameObjectWithTag("Scared");
        target = player.transform;
    }
    void Update()
    {
        switch (attackMode)
        {
            case AttackMode.Normal:
                {
                    //Patroll and move
                    if (motor.RotateTowards(wayPoints[currentPoint].position, data.turnSpeed))
                    {
                        //Do nothing because it is still turning
                    }
                    else // Move foward when faceing it
                    {
                        motor.Move(data.moveSpeed);
                    }
                    // If we are close to the waypoint,
                    if (Vector3.SqrMagnitude(wayPoints[currentPoint].position - tf.position) < (closeEnough * closeEnough))
                    {
                        switch (patrollType) //Check whitch state 
                        {
                            case PatrollType.Stop:
                                {//check range position then incriment
                                    if (currentPoint < wayPoints.Length - 1)
                                    {
                                        currentPoint++;
                                    }
                                }
                                break;
                            case PatrollType.Loop:
                                {//Check position in range 
                                    if (currentPoint < wayPoints.Length - 1)
                                    {
                                        currentPoint++;
                                    }
                                    else
                                    {
                                        //Otherwise go to waypoint 0
                                        currentPoint = 0;
                                    }
                                }
                                break;
                            case PatrollType.PingPong:
                                {
                                    if (isPatrolForward)// Check to see direction 
                                    {
                                        // Move to the next point
                                        if (currentPoint < wayPoints.Length - 1)
                                        {
                                            currentPoint++;
                                        }
                                        else
                                        {
                                            //Reverse direction 
                                            isPatrolForward = false;
                                            currentPoint--;
                                        }
                                    }
                                    else
                                    {
                                        // Move to the next point
                                        if (currentPoint > 0)
                                        {
                                            currentPoint--;
                                        }
                                        else
                                        {
                                            //Reverse direction 
                                            isPatrolForward = true;
                                            currentPoint++;
                                        }
                                    }
                                }
                                break;
                            default: break;
                        }
                    }
                }
                break;//Go back to patroll.
            case AttackMode.Chase:
                {
                    if (avoidanceStage != 0)
                    {
                        DoAvoidance();
                    }
                    else
                    {
                        DoChase(target.position);
                    }

                    if (data.health <= 50 && tf.gameObject == scared.gameObject)
                    {
                        ChangeState(AttackMode.Flee);
                    }
                    else if (Vector3.SqrMagnitude(target.position - tf.position) < (closeEnough * closeEnough))
                    {
                        ChangeState(AttackMode.Shoot);
                    }
                    else if (Vector3.SqrMagnitude(target.position - tf.position) < (chaseDistance * chaseDistance))
                    {
                        ChangeState(AttackMode.Chase);
                    }
                    else
                    {
                        ChangeState(AttackMode.Normal);
                    }
                }
                break;
            case AttackMode.Flee:
                {
                    if (avoidanceStage != 0)
                    {
                        DoAvoidance();
                    }
                    else
                    {
                        DoChase(-target.position);
                    }

                    if (data.health <= 50)
                    {
                        ChangeState(AttackMode.Flee);
                    }
                    else if (Vector3.SqrMagnitude(target.position - tf.position) < (chaseDistance * chaseDistance))
                    {
                        ChangeState(AttackMode.Chase);
                    }
                    else
                    {
                        ChangeState(AttackMode.Normal);
                    }
                }
                break;
            case AttackMode.Shoot:
                {
                    motor.Shoot();

                    if (Vector3.SqrMagnitude(target.position - tf.position) < (closeEnough * closeEnough)) {
                        ChangeState(AttackMode.Shoot);
                    }
                    else
                    {
                        ChangeState(AttackMode.Normal);
                    }
                }
                break;
            default: break;
        }
    }
    public bool CanMove(float speed)
    {
        RaycastHit hit;
        
        //If player is hit then stop and shoot.
        if (Physics.Raycast(tf.position, tf.forward, out hit, speed))
        {
            // ... and if what we hit is not the player...
            if (!hit.collider.tag.Equals("Player"))
            { 
                return false;
            }
            else if (Vector3.SqrMagnitude(target.position - tf.position) < (closeEnough * closeEnough))
            {
                ChangeState(AttackMode.Shoot);
            }
            ChangeState(AttackMode.Normal);
        }
        // otherwise, we can move, so return true
        return true;
    }
    void DoChase(Vector3 target)
    {
        motor.RotateTowards(target, data.turnSpeed);
        // Check if we can move "data.moveSpeed" units away.
        //    We chose this distance, because that is how far we move in 1 second,
        //    This means, we are looking for collisions "one second in the future."
        if (CanMove(data.moveSpeed))
        {
            motor.Move(data.moveSpeed);
        }
        else
        {
            // Enter obstacle avoidance stage 1
            avoidanceStage = 1;
        }
    }
    void DoAvoidance()
    {
        if (avoidanceStage == 1)
        {
            // Turn left
            motor.Turn(-1 * data.turnSpeed);

            // If I can now move forward, move to stage 2!
            if (CanMove(data.moveSpeed))
            {
                avoidanceStage = 2;
                exitTime = avoidanceTime;
            }

            // Otherwise, we'll do this again next turn!
        }
        else if (avoidanceStage == 2)
        {
            // if we can move forward, do so
            if (CanMove(data.moveSpeed))
            {
                // Subtract from our timer and move
                exitTime -= Time.deltaTime;
                motor.Move(data.moveSpeed);

                // If we have moved long enough, return to chase mode
                if (exitTime <= 0)
                {
                    avoidanceStage = 0;
                }
            }
            else
            {
                // Otherwise, we can't move forward, so back to stage 1
                avoidanceStage = 1;
            }
        }
    }
    public void ChangeState(AttackMode newState)
    {
        // Change our state
        attackMode = newState;
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject == player.gameObject)
        {

            switch (personality)
            {
                case Personality.Normal:
                    {
                        //Do nothig
                    }
                    break;
                case Personality.Gaurd:
                    {
                        if (motor.RotateTowards(wayPoints[currentPoint].position, data.turnSpeed))
                        {
                            //Do nothing because it is still turning
                        }
                        else // Shoot
                        {
                            motor.Shoot();
                        }
                    }
                    break;
                case Personality.Chaser:
                    {
                        ChangeState(AttackMode.Chase);
                    }
                    break;
                case Personality.Scared:
                    {
                        ChangeState(AttackMode.Flee);
                    }
                    break;
            }

        }
        else
        {
            //Do Nothig
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject == player.gameObject)
        {
            ChangeState(AttackMode.Normal);
            if (tf.tag.Equals("Normal"))
            {
                patrollType = PatrollType.Stop;
            }
            if (tf.tag.Equals("Gaurd"))
            {
                patrollType = PatrollType.PingPong;
            }
            if (tf.tag.Equals("Chaser"))
            {
                patrollType = PatrollType.Loop;
            }
            if (tf.tag.Equals("Scared"))
            {
                patrollType = PatrollType.Loop;
            }
        }
    }
}